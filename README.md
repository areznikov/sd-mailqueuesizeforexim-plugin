sd-MailQueueSizeForExim-plugin
==============================

Plugin requires sudo access, so please follow ServerDensity knowledgebase http://support.serverdensity.com/knowledgebase/articles/112413-plugins-requiring-sudo
and add
sd-agent ALL=(ALL) NOPASSWD: /usr/sbin/exim

If you have exim path different from /usr/sbin/exim, please add a following entry to /etc/sd-agent/config.cfg
plugin_mailqueuesizeforexim_eximpath: /usr/local/sbin/exim
with proper exim path


