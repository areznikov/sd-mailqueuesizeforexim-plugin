"""Server Density plugin for monitoring exim mail queue size currently on the server 

Copyright 2011  Artur Reznikov areznikov@live.com. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other materials
provided with the distribution.

THIS SOFTWARE IS PROVIDED BY ARTUR REZNIKOV ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ARTUR REZNIKOV OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Artur Reznikov.
"""

__author__ = "Artur Reznikov <areznikov@live.com>"
__version__ = "1.0.1"

import subprocess

class MailQueueSizeForExim:
    def __init__(self, config, logger, raw_config):
        self.config = config
        self.logger = logger
        self.raw_config = raw_config

    def run(self):
	exim_path = self.raw_config['plugin_mailqueuesizeforexim_eximpath'] if self.raw_config.has_key('plugin_mailqueuesizeforexim_eximpath') else '/usr/sbin/exim'
	command_to_run = 'sudo ' + exim_path + ' -bpc'
        queue_size = int(subprocess.Popen(command_to_run,stdout=subprocess.PIPE, shell=True).communicate()[0].strip())
        return_data = {'MailQueueSize': queue_size }
        return return_data
